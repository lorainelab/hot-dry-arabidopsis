Effects of heat and drought on cytokinin pathway genes
========================================================

Introduction
------------

Cytokinin synthesis genes were up-regulated in the drought stress experiment, suggesting there were elevated levels of cytokinin in the treated plants. If yes, and if these elevated levels were physiologically relevant, we should observe up-regulation of cytokinin target genes, unless there is also negative feedback occurring. 

In this Markdown, we'll answer the following questions:

* Are genes known to be regulated by cytokinin (up or down) differentially expressed in the drought stress experimental samples?

Analysis
--------

Load cytokinin response genes:

```{r}
suppressPackageStartupMessages(library(xlsx))
genes=read.xlsx2('data/GoldenListFromPaper.xlsx',1)
# last two rows of spreadsheet are empty
genes=genes[1:226,]
# AGIs have whitespace around them 
library(stringr)
x=str_replace_all(as.character(genes$AGI),'\\s+','')
genes$AGI=x
row.names(genes)=genes$AGI
```

Load DE results from drought stress:

```{r}
fdr=0.001
dry=read.delim('results/WetDry.tsv.gz',as.is=T)
row.names(dry)=dry$AGI
both=merge(genes,dry[,c('AGI','logFC','p.adj')],
           by.x='AGI',by.y='AGI',all.x=T)
both.de=both[both$p.adj<=fdr&!is.na(both$p.adj),]
```

There were `r nrow(both)` genes on the golden list of cytokinin-responsive genes.

Of these, `r round(nrow(both.de)/nrow(both)*100,1)` were also DE in the drought experiment. Overall, only about 18% of genes were DE in the genome, and among the genes that were actually expressed, `r round(length(which(dry$p.adj<=0.001))/nrow(dry)*100,0)`% were DE. Thus, the known cytokinin responders appeared to contain an usually high number of DE genes from the drought experiment.

There were several type A RR's that were DE in drought. These were:

```{r}
v=grep('ARR',both.de$Symbol)
arrs=both.de[v,]
arrs
```

Note that all were down-regulated by drought. This suggests that the drought stress enhances the capability of cells to respond to endogenous cytokinin. 

Were the DE genes changed in the same direction as the golden list?

```{r}
v=rep(0,nrow(both.de))
v[both.de$UP_Down=='Up']=1
v[both.de$UP_Down=='Down']=-1
both.de$cyto=v
v=rep(0,nrow(both.de))
v[both.de$logFC<0]=-1
v[both.de$logFC>0]=1
both.de$dry=v
```

Of the `r nrow(both.de)` DE genes, `r sum(no.arrs$cyto*no.arrs$logFC<0)` were regulated in opposited directions in the cytokinin versus drought stress data set. 

That is, there were 9 genes that were down in both the cytokinin responders set and in the drought set, 19 where they were both up, 16 where drought was up and cytokinin was down, and 45 where drought was down and cytokinin was up. 

A fisher's test of a contingency table gave p value of 0.004, confirming that the ratio of negative to positive regulation was very different in the two datasets. 

This is not suprising because most of the regulation under drought stress was negative while most of the regulation reported in the cytokinin reponders was positive. Also, the cytokinin responders list comes from experiments in which the cytokinin application was of short duration and where samples were collected with a few hours of the application. 

Conclusion
----------

The cytokinin responders were enriched among the DE genes, but the direction of differential expression was inconsistent, probably reflecting the fact that the abundance of up versus down-regulated genes was different in the two groups and the differences in experimental design.

Limitations of the analysis
---------------------------

Genes that were added to the cytokinin responders list got there because they were up- or down-regulated upon short-term application of exogenous cytokinin to seedlings. The short-term, immediate cytokinin response is likely very different from how the pathway functions in 21-day old plants undergoing severe drought stress.

Session info
------------

```{r}
sessionInfo()
```