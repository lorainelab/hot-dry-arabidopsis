Differential Expression Analysis of Severe Drought Stress
========================================================

Questions:

* How many genes were differentially expressed?
* How many genes were up-regulated?
* How many genes were down-regulated?

Analysis
-------------------------

### Load data 

```{r}
source('../src/common.R')
counts=getCounts()
samples=getDroughtT2Samples()
counts=counts[,samples]
group=rep('W',length(grep('^W',samples)))
group=c(group,rep('D',length(grep('Dry',samples))))
```

Make object for EdgeR/Bioconductor analysis:

```{r warning=FALSE,message=FALSE}
suppressPackageStartupMessages(library(edgeR))
dge=DGEList(counts,group=group,remove.zeros=TRUE)
dge=calcNormFactors(dge)
```

### Sample Clustering

#### Multi-dimensional scaling

```{r fig.height=4,fig.width=4}
source('../src/common.R')
colors=getColors(colnames(dge$counts))
main='Drought stress'
plotMDS(dge,main=main,labels=colnames(dge$counts),col=colors,xlim=c(-6,5),ylim=c(-3,2))
```

Dimension 1 separates control and treatment samples.

#### Hierarchical clustering

```{r fig.width=4,fig.height=4}
normalized.counts=cpm(dge)
transposed=t(normalized.counts) # transpose counts matrix
distance=dist(transposed) # calculate distance
clusters=hclust(distance) # does hierarchical clustering
makeClusterPlot(clusters,lab.col=getColors(clusters$labels))
```

Treatment and control samples cluster separately.

### Differential Expression Analysis

```{r}
dge=estimateCommonDisp(dge)
dge=estimateTagwiseDisp(dge)
dex=exactTest(dge,dispersion="tagwise",pair=c("W","D"))
```

Use the summary and decideTestsDGE commands to find out how many genes are up- or down-regulated using different Benjamini-Hockberg adjusted FDR cutoffs:

```{r}
summary(decideTestsDGE(dex,p=0.05))
summary(decideTestsDGE(dex,p=0.01))
summary(decideTestsDGE(dex,p=0.005))
summary(decideTestsDGE(dex,p=0.001))
summary(decideTestsDGE(dex,p=0.0001))
```

Based on this, pick a cutoff:

```{r}
fdr.cutoff=0.0001
```

### Get an overview of the DE genes in the data set

Use plotSmear to view the relationship between logFC and average expression level for the differentially expressed genes, which are highlighted in red. The blue lines indicate genes that are up or downregulated two fold.

```{r fig.width=4,fig.height=4}
de = decideTestsDGE(dex, p = fdr.cutoff, adjust = "BH")
detags = rownames(dex)[as.logical(de)]
plotSmear(dex, de.tags = detags)
abline(h = c(-1, 1), col = "blue")
```

Most DE genes are changed more than two-fold.

### Report results

Read RPKM file:

```{r}
RPKM=getRpkm()
aves=RPKM[,grep('ave',names(RPKM),value=T)]
aves$gene=RPKM$gene
aves$descr=RPKM$descr
```

Write results:

```{r}
res=data.frame(gene=row.names(dex$table),
              fdr=p.adjust(dex$table$PValue,method='BH'),
              logFC=dex$table$logFC)
res=merge(res,aves[,c('gene','WetT2.ave','DryT2.ave','descr')],
          by.x='gene',by.y='gene')
res=res[order(res$fdr),]
fname='results/WetDry.txt'
write.table(res,file=fname,row.names=F,sep='\t',quote=F)
system(paste('gzip -f',fname))
```

Top ten most differentially expressed genes:

```{r}
res[1:10,c('gene','logFC','descr')]
```

### Should we use all the control samples?

```{r fig.width=4,fig.height=4}
control=getAllControlSamples()
dry=grep('Dry',getDroughtT2Samples(),value=T)
samples=c(control,dry)
counts=getCounts()[,samples]
group=c(rep('W',length(control)),rep('D',length(dry)))
dge2=DGEList(counts,group=group,remove.zeros=TRUE)
dge2=calcNormFactors(dge2)
colors=getColors(colnames(dge2$counts))
main='Drought stress'
plotMDS(dge2,main=main,labels=colnames(dge2$counts),col=colors,xlim=c(-5,6))
```

Dimension 1 separates drought-stressed and control samples, but dimension 2 separates the heat stress and drought stress controls.

In light of this, treating controls as equivalent is probably not appropropriate.

View sample clustering dendrogram:

```{r fig.width=4,fig.height=4}
normalized.counts=cpm(dge2)
transposed=t(normalized.counts) # transpose counts matrix
distance=dist(transposed) # calculate distance
clusters=hclust(distance) # hierarchical clustering
colors=getColors(clusters$labels)
makeClusterPlot(clusters,lab.col=colors)
```

How does including all controls affect analysis?

```{r}
dge2=estimateCommonDisp(dge2)
dge2=estimateTagwiseDisp(dge2)
dex2=exactTest(dge2,dispersion="tagwise",
              pair=c("W","D"))
```

Use the summary and decideTestsDGE commands to find out how many genes are up- or down-regulated using different Benjamini-Hockberg adjusted FDR cutoffs:

```{r}
summary(decideTestsDGE(dex2,p=0.05))
summary(decideTestsDGE(dex2,p=0.01))
summary(decideTestsDGE(dex2,p=0.005))
summary(decideTestsDGE(dex2,p=0.001))
summary(decideTestsDGE(dex2,p=0.0001))
```

There are many more DE genes detected when all controls are used, but I don't think it is appropriate to treat the controls as equivalent.

### Compare the controls to each other

If the control samples were truly equivalent, there should not be many DE genes when we compare CoolT1/2 and WetT2 samples. The WetT2 samples were a few days older, but were collected at about the same time of day. We might observe some genes related to age and possibly transition to reproductive development in the older, WetT2 samples. There might be differences related to how the samples were collected, such as the temperature of the room, the amount of light, etc. 

```{r fig.width=4,fig.height=4}
wet=grep('Wet',samples,value=T)
cool=grep('Cool',samples,value=T)
samples=c(cool,wet)
counts=getCounts()[,samples]
group=c(rep('C',length(cool)),rep('W',length(wet)))
dge3=DGEList(counts,group=group,remove.zeros=TRUE)
dge3=calcNormFactors(dge3)
colors=getColors(colnames(dge3$counts))
main='Controls'
plotMDS(dge3,main=main,labels=colnames(dge3$counts),col=colors,xlim=c(-3,4),ylim=c(-2,2))
dge3=estimateCommonDisp(dge3)
dge3=estimateTagwiseDisp(dge3)
dex3=exactTest(dge3,dispersion="tagwise",
              pair=c("C","W"))
summary(decideTestsDGE(dex3,p=0.05))
summary(decideTestsDGE(dex3,p=0.01))
summary(decideTestsDGE(dex3,p=0.005))
summary(decideTestsDGE(dex3,p=0.001))
summary(decideTestsDGE(dex3,p=0.0001))
```

There were many fewer DE genes, but enough to indicate the control samples are not equivalent. For further differential expression analysis, use results from comparing the T2 drought experiment samples only.

Conclusion
==========

Of `r nrow(dge$counts)` expressed genes (with at least one overlapping read), `r sum(res$fdr<=fdr.cutoff)` genes (`r round(sum(res$fdr<=fdr.cutoff)/nrow(dge$counts)*100,1)`%) were differentially expressed. 

* `r sum(res$fdr<=fdr.cutoff&res$logFC>0)` were up-regulated.
* `r sum(res$fdr<=fdr.cutoff&res$logFC<0)` were down-regulated.
* `r round(100*sum(res$fdr<=fdr.cutoff)/nrow(counts),1)`% of the expressed transcriptome was DE

At least for now, we won't combine the drought stress T2 and heat stress T1 and T2 controls for DE analysis.

Session Information
===================

```{r}
sessionInfo()
````

