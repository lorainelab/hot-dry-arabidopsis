Creating RPM and RPKM files
===========================

Introduction
------------
This file creates RPM (counts per million) and RPKM (counts per million thousand) with expression data for every annotated gene.

The RPM file is useful for comparing expression across samples and the RPKM file is useful comparing expression between genes within the same sample. 

### Load data

Load counts per gene:

```{r}
fname='results/heat_drought_counts.tsv.gz'
counts=read.delim(fname,header=T,sep='\t')
rownames(counts)=counts$gene
counts=counts[,-which(names(counts)=='gene')]
```

Get annotations:

```{r}
annotsfile='../ExternalDataSets/genedescr.tsv.gz'
annots=read.delim(annotsfile,header=T,sep='\t',quote="")
rownames(annots)=annots$AGI
```


### Mapped reads

```{r}
sums=apply(counts,2,sum)
names(sums)=names(counts)
sapply(sums,function(x)round(x/10**6,1))
```

We mapped `r round(sum(sums)/10**6,1)` million reads onto the genome. These reads:

* mapped exactly once

### Make reads per million (RPM) table

```{r}
RPM=counts
for (sample in names(RPM)) {
  v=RPM[,sample]/sums[sample]*10**6
  RPM[,sample]=v
}
source('../src/common.R')
sample_types=getGroups()
for (sample_type in unique(sample_types)) {
  samples=names(sample_types[sample_types==sample_type])
  ave=apply(RPM[,samples],1,mean)
  RPM=cbind(RPM,ave)
  names(RPM)[ncol(RPM)]=paste0(sample_type,'.ave')
  sd=apply(RPM[,samples],1,sd)
  RPM=cbind(RPM,sd)
  names(RPM)[ncol(RPM)]=paste0(sample_type,'.sd')
}
```

Write results:

```{r}
f='results/heat_drought_RPM_all.tsv'
to_write=RPM
to_write$gene=row.names(RPM)
to_write=merge(to_write,annots,by.x='gene',by.y='AGI')
write.table(to_write,file=f,row.names=F,
            sep='\t',quote=F)
system(paste('gzip -f',f))
```

### Make RPKM table

Read transcript sizes:

```{r}
sizesfile='../ExternalDataSets/tx_size.txt.gz'
sizes=read.delim(sizesfile,header=T,sep='\t')
sizes$kb=sizes$bp/1000
sizes=sizes[,c('locus','kb')]
row.names(sizes)=sizes$locus
sizes=sizes[row.names(counts),'kb']
```

Divide by size (kb):

```{r}
RPKM=RPM[,names(counts)]
for (sample in names(RPKM)) {
  RPKM[,sample]=RPKM[,sample]/sizes
}
for (sample_type in unique(sample_types)) {
  samples=names(sample_types[sample_types==sample_type])
  ave=apply(RPKM[,samples],1,mean)
  RPKM=cbind(RPKM,ave)
  names(RPKM)[ncol(RPKM)]=paste0(sample_type,'.ave')
  sd=apply(RPKM[,samples],1,sd)
  RPKM=cbind(RPKM,sd)
  names(RPKM)[ncol(RPKM)]=paste0(sample_type,'.sd')
}
to_write=RPKM
to_write$gene=row.names(RPKM)
to_write=merge(to_write,annots,by.x='gene',
               by.y='AGI')
```

Write file:

```{r}
f='results/heat_drought_RPKM_all.tsv'
write.table(to_write,file=f,row.names=F,
            sep='\t',quote=F)
system(paste('gzip -f',f))
```

### Removing samples with low read diversity

Visualization of read alignments in IGB identified samples with low read complexity or very low read coverage. 

Get low-complexity and/or low coverage samples and recalculate average RPM and RPKM without those samples:

```{r}
toExclude=union(getPcrDups(),getLowAbundance())
indexes=which(names(counts)%in%toExclude)
counts=counts[,-indexes]
sums=sums[-indexes]
RPM=counts
for (sample in names(RPM)) {
  v=RPM[,sample]/sums[sample]*10**6
  RPM[,sample]=v
}
sample_types=getGroups(names(RPM))
for (sample_type in unique(sample_types)) {
  samples=names(sample_types[sample_types==sample_type])
  ave=apply(RPM[,samples],1,mean)
  RPM=cbind(RPM,ave)
  names(RPM)[ncol(RPM)]=paste0(sample_type,'.ave')
  sd=apply(RPM[,samples],1,sd)
  RPM=cbind(RPM,sd)
  names(RPM)[ncol(RPM)]=paste0(sample_type,'.sd')
}
f='results/heat_drought_RPM.tsv'
to_write=RPM
to_write$gene=row.names(RPM)
to_write=merge(to_write,annots,by.x='gene',by.y='AGI')
write.table(to_write,file=f,row.names=F,
            sep='\t',quote=F)
system(paste('gzip -f',f))
RPKM=RPM[,names(counts)]
for (sample in names(RPKM)) {
  RPKM[,sample]=RPKM[,sample]/sizes
}
for (sample_type in unique(sample_types)) {
  samples=names(sample_types[sample_types==sample_type])
  ave=apply(RPKM[,samples],1,mean)
  RPKM=cbind(RPKM,ave)
  names(RPKM)[ncol(RPKM)]=paste0(sample_type,'.ave')
  sd=apply(RPKM[,samples],1,sd)
  RPKM=cbind(RPKM,sd)
  names(RPKM)[ncol(RPKM)]=paste0(sample_type,'.sd')
}
to_write=RPKM
to_write$gene=row.names(RPKM)
to_write=merge(to_write,annots,by.x='gene',
               by.y='AGI')
f='results/heat_drought_RPKM.tsv'
write.table(to_write,file=f,row.names=F,
            sep='\t',quote=F)
system(paste('gzip -f',f))
```

Sanity-check RPM:

```{r fig.width=7,fig.height=5}
g='AT1G07350'
means=RPM[g,grep('.ave',names(RPM))]
labels=sub('.ave','',names(means))
names(means)=labels
stdevs=RPM[g,grep('.sd',names(RPM))]
colors=getColors(labels)
makeBarPlot(means,stdevs,xlab="Samples",ylab="RPM",main=g,col=colors,names=labels)
```


Sanity-check RPKM:

```{r fig.width=7,fig.height=5}
means=RPKM[g,grep('.ave',names(RPKM))]
labels=sub('.ave','',names(means))
stdevs=RPKM[g,grep('.sd',names(RPKM))]
makeBarPlot(means,stdevs,xlab="Samples",ylab="RPKM",main=g,col=colors,names=labels)
```


Summary
-------

Wrote RPM and RPKM data files needed for other analyses.

Limitations
-----------

* Some reads from overlapping genes were counted more than once. These were not included in the denominators used to normalize. 

Session information
-------------------


```{r}
sessionInfo()
````




