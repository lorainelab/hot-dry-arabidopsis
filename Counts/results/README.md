# Counts/results contents

## Made by Counts.Rmd:

* AT1G07350_RPM.pdf - barchart showing expression of SR45a, induced by drought and heat
* sequencingDepth.pdf - barchart showing reads mapped per library, counting reads that map to genes only

## Made by MakeRpmRpkmFiles.Rmd

Expression values for TAIR10 genes:

* heat_drought_RPM.tsv.gz - Reads per million in each sample for TAIR10 genes
* heat_drought_RPKM.tsv.gz - Reads per million per kilobase of transcript mapped 
 
