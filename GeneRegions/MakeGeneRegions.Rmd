Making gene region file to count overlapping reads
=========================================

```{r echo=FALSE}
species='Arabidopsis thaliana'
annots_url='http://igbquickload.org/quickload/A_thaliana_Jun_2009/Araport11.bed.gz'
annots_file='Araport11.bed.gz'
if (! file.exists(annots_file)) {
  download.file(annots_url,annots_file)
}
```

Introduction
------------
  
We will use [featureCounts](http://bioinf.wehi.edu.au/featureCounts/) to count reads or read pairs that overlap a gene or other feature. The program accepts a simple format called SAF which consists of 

* gene name
* chromosome name
* start 
* end
* strand

Here, we'll make this file for `r species`.

Questions:

* How many genes are there?
* How many splice variants are there?

Analysis
--------
  
Read the file with gene models and get columns we need to define gene regions:
  
```{r}
source('../src/common.R')
annots=read.delim(annots_file,header=F,as.is=T,sep='\t')
annots=annots[,c(1,2,3,4,6,13)]
names(annots)=c('seq','start','end','tx_id','strand','locus')
annots$locus=varToLocusId(annots$tx_id)
o=order(annots$seq,annots$start)
annots=annots[o,]
minstarts=annots[!duplicated(annots$locus),
                 c('locus','seq','start')]
o=order(annots$seq,annots$end,decreasing=T)
annots=annots[o,]
maxends=annots[!duplicated(annots$locus),
               c('locus','end','strand')]
saf=merge(minstarts,maxends,by.x='locus',by.y='locus')
saf=saf[,c('locus','seq','start','end','strand')]
saf$end=saf$end+1 # saf is one-based 
lens=saf$end-saf$start
names(lens)=saf$locus
```

There were `r nrow(annots)` gene models belong to `r nrow(saf)` `r species` locus annotations.

The largest gene had size of `r max(lens)` and the smallest gene had size of `r min(lens)`. 

Examine the distribution of sizes:
  
```{r fig.width=7,fig.height=5}
par(las=1)
main="Histogram of gene sizes"
ylab="base pairs"
hist(log10(lens))
```

Write the SAF file to disk.

```{r}
fname='results/SAF.txt'
write.table(saf,file=fname,col.names=F,row.names=F,
sep='\t',quote=F)
```

Conclusion
----------

There were `r nrow(annots)` gene models annotations in annotation file `r annots_file` representing `r nrow(saf)` genes.

Session info
============

```{r}
sessionInfo()
````
