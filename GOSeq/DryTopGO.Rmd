Gene Ontology and AraCyc enrichment analysis for Drought Stress Experiment
========================================================

Introduction
-------------

This Markdown uses the TopGO package from Bioconductor to identify GO categories that contained unusually many DE genes in the drought stress data set. 

This complements/reinforces the GOSeq based analysis in DryGOSeq.Rmd.

Questions:

* Which pathways contained unsually large numbers of DE genes?
* How similar were results to output of GOSeq?

* * *

Analysis
--------

### Setting up

Load libraries:

```{r message=FALSE}
# source('http://www.bioconductor.org/biocLite.R')
# biocLite('topGO')
suppressPackageStartupMessages(library(topGO))
suppressPackageStartupMessages(library(org.At.tair.db))
source('../src/common.R')
```

Get results from DE analysis:

```{r}
d=getDroughtDiffExprResults()
d=d[,c('gene','fdr')]
```

Get the entire gene universe; all the genes we tested orginally:

```{r}
all=read.delim('../ExternalDataSets/tx_size.txt.gz',as.is=T,header=T)
```

Combine them, assigning FDR of 1 for genes that were previously filtered because they had zero expression. 

Also, analyze up and down-regulated genes separately in addition to all of them at once.

```{r}
all=merge(all,d,by.x='locus',by.y='gene',all.x=T)
v=which(is.na(all$fdr))
all[v,c('fdr')]=1
```

Create named numeric vector containing fdr values needed as input to topGO functions:

```{r}
geneList=all$fdr
names(geneList)=all$locus
```

Define a function that selects differentially expressed genes:

```{r}
topDiffGenes = function(allScore) {
  return(allScore<0.005) # FDR threshold for DE genes
}
```

Find out how many Arabidopsis genes had GO term annotations:

```{r}
x = org.At.tairGO
mapped_genes = mappedkeys(x)
```

In Arabidopsis, there were `r length(mapped_genes)` genes with GO annotations.

Now, build a topGOdata objects which we'll use for GO enrichment analysis:

```{r}
mf = new("topGOdata",
          description="GO analysis of drought stress",
          ontology="MF",
          allGenes=geneList,
          geneSel=topDiffGenes,
          nodeSize=5,
          mapping="org.At.tair.db",
          annot=annFUN.org)
bp = new("topGOdata",
          description="GO analysis of drought stress",
          ontology="BP",
          allGenes=geneList,
          geneSel=topDiffGenes,
          nodeSize=5,
          mapping="org.At.tair.db",
          annot=annFUN.org)
```              

Test using class Fisher's Exact test and Kolmogorov test:

```{r}
fisher=new("classicCount",
            testStatistic=GOFisherTest,
            name="Fisher test")
resultFisher.bp=getSigGroups(bp,fisher)
resultFisher.mf=getSigGroups(mf,fisher)
ks=new("classicScore",
        testStatistic=GOKSTest,
        name="KS tests")
resultKS.bp=getSigGroups(bp,ks)
resultKS.mf=getSigGroups(mf,ks)
res.bp=GenTable(bp,classic=resultFisher.bp,
                KS=resultKS.bp,orderBy="KS",
                ranksOf='classic',topNodes=50)
res.mf=GenTable(mf,classic=resultFisher.mf,
                KS=resultKS.mf,orderBy="KS",
                ranksOf='classic',topNodes=50)
````

Mostly the same set of Biological Process terms were significant for both the KS and Fisher tests, but there were more differences between the results for these tests and the Molecular Function sub-tree. 

Terms related to stimulus and stimulus responses were significant, along with terms related to transcriptional regulation, catabolism, and hormone signaling and/or transport. I interpret this as meaning that the plants responded to the stress by activating catabolic pathways as well as by modulating hormone signaling and transcriptional regulation of genes.

* * *

Conclusion
----------

Many terms were enriched with differentially expressed genes. Results seemed mostly consistent with results from the GOSeq analysis. However, there were so many signficant GO terms in both analyses that it was hard to interpret the results.


* * *

Session Information
-------------------

```{r}
sessionInfo()
```
