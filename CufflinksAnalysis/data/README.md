# Transcript Assembly Using Cufflinks

Single-mapping reads were combined into a single BAM file using
samtools merge, created heat_drought.bam, which contained 508,448,758
alignments. BAM files that were merged were created using tophat 2.0.5.

Following this, cufflinks was used to merge alignments into transcript
models. Cufflinks was run twice, once using Arabidopsis TAIR10 gene
annotations to guide the alignment (PlusG) and once using no annotations.

Cufflinks output files: 

* transcripts.NoG.gtf - no gene models used
* transcripts.PlusG.gtf - gene models used
annotations (NoG).

Cufflinks version used was v2.2.1. The scripts used to run cufflinks
were

* src/runCufflinksNoG.sh
* src/runCufflinks.sh

Annotations were TAIR.gtf.gz in ExternalDataSets.


