Comparing samples 
==================

Introduction
------------

This analysis determines similarity between samples.

Questions:

* Are controls similar?
* Are the recovered (T2) heat treated samples similar to untreated controls?
* How does removing samples with low read diversity (due to PCR duplicates) affect clustering?

* * *

Analysis
--------

Read RPKM expression values, per gene, per libary:

```{r}
source('../src/common.R')
d=getRpkmAll()
```

### Clustering - all samples

```{r fig.width=6.5,fig.height=3}
samples=getSamples()
dataFrame=t(d[,samples])
distAS=dist(dataFrame)
hc=hclust(distAS)
colors=getColors(samples)
main='Clustering by Expression (RPKM)'
par(mar=c(3,0,2,0))
makeClusterPlot(hc,lab.col=colors,main=main,axes=F)
figfile='results/Hclust_RPKM.pdf'
quartz.save(file=figfile,width=6.5,height=3,type="PDF")
```

### Clustering - removing low read diversity samples

```{r fig.width=6.5,fig.height=3}
samples=getSamples()
pcrDups=getPcrDups()
samples=samples[which(!samples%in%pcrDups)]
dataFrame=t(d[,samples])
distAS=dist(dataFrame)
hc=hclust(distAS)
colors=getColors(samples)
main='Clustering by Expression (RPKM)'
par(mar=c(3,0,2,0))
makeClusterPlot(hc,lab.col=colors,main=main,axes=F)
figfile='results/Hclust_NoDups_RPKM.pdf'
quartz.save(file=figfile,width=6.5,height=3,dpi=600,type="PDF")
```


Session info
------------

```{r}
sessionInfo()
```

