Assembling RNA-Seq reads with cufflinks
========================================================

Introduction
------------

Cufflinks can assemble transcript models using RNA-Seq read alignments.

This Markdown writes a script for merging BAM files.

Methods
-------

```{r}
fname='../src/mergeBam.sh'
cat('#!/bin/sh\n',file=fname)
cat("samtools merge heat_drought.bam ",file=fname,append=T)
WetDry=getDroughtT2Samples()
HotT1=getHotT1Samples()
HotT2=getHotT2Samples()
all=c(WetDry,HotT1,HotT2)
all=paste(paste0(all,'.sm.bam'),collapse=' ')
cat(all,file=fname,append=T)
cat('\n',file=fname,append=T)
```

This was helpful:

http://stackoverflow.com/questions/2470248/write-lines-of-text-to-a-file-in-r