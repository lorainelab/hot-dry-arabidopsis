#!/usr/bin/env python

# This code generates an annots.xml file specifying
# colors, track names, file names and other settings
# for data to be shown in Integrated Genome Browser

# Imported module code is available from:
# https://bitbucket.org/lorainelab/igbquickload

from Quickload import main
from AnnotsXmlForRNASeq import makeQuickloadFilesForRNASeq
from QuickloadUtils import checkForHelp

import os,sys,re,urllib2

genome="A_thaliana_Jun_2009"

# name of directory on the server containing the files
rnaseq_deploy_dir='SRP220157'

# track information URL; where users go if click "i" in Data Access Panel
# if not using absolute URL, link is relative to quickload site root directory
track_url="https://bitbucket.org/lorainelab/hot-dry-arabidopsis"

# make a objects representing quickload data files
def makeQuickloadFiles(fname='../ExternalDataSets/samples.txt'):
    samples=makeSamplesList(fname=fname)
    quickload_files = makeQuickloadFilesForRNASeq(lsts=samples,
                                                  folder="Hot dry SRP220157",
                                                  deploy_dir=rnaseq_deploy_dir,
                                                  include_scaled_bw=True,
                                                  include_unscaled_bedgraph=True,
                                                  include_TH=True,
                                                  include_FJ=True)
    return quickload_files

# make list of lists, depends on a sample description file located
# in a sister directory (ExternalDataSets)
# permanent (kind of) link to same is https://bitbucket.org/lorainelab/hot-dry-arabidopsis/raw/master/ExternalDataSets/samples.txt
# makeRnaSeqQL in git@bitbucket.org:lorainelab/igbquickload.git expects this URL to exist
def makeSamplesList(fname='../ExternalDataSets/samples.txt'):
    if fname.startswith("http"):
        lines = urllib2.urlopen(fname).readlines()
    else:
        lines=open(fname,'r').readlines()
    lines = filter(lambda x:x[0]!='#',lines) # ignore comment lines
    header=lines[0]
    num_toks=len(header.split(','))
    lines = lines[1:] # ignore header (first) line
    tokss = map(lambda x:x.rstrip().split(','),lines)
    to_return=[]
    # toks is a list, tokss is a list of lists
    for toks in tokss:
        if len(toks)!=num_toks: # hand editing may add empty line at end of samples.txt
            continue
        group = toks[2]
        fname_prefix = toks[7] # SRR accession
        sample = group + " " + toks[7] # e.g., CoolT1 SRR10060904
        tooltip = toks[1] + " (" + toks[0] + ")" # e.g., Pot L Collection 1 Time 1 Cool (CoolL1T1)"
        tooltip = "Group " + group + " - " + tooltip
        track_color=re.sub(r'#','',toks[3])
        low=toks[4] # very low overall sequencing depth
        pcr=toks[5] # PCR duplicates due to overamplification
        extra_bit=''
        if low=='TRUE' and pcr=='TRUE':
            extra_bit="(low count, PCR dup)"
            tooltip = tooltip + ", low count and PCR duplicates"
        elif low=='TRUE':
            extra_bit="(low count)"
            tooltip = tooltip + ", low count"
        elif pcr=='TRUE':
            extra_bit='(PCR dup)'
            tooltip = tooltip + ", PCR duplicates"
        else:
            extra_bit="(data OK)"
            tooltip = tooltip + ", data OK"
        track_name = "%s %s"%(sample,extra_bit)
        track_background_color="ffffff"
        lst=[fname_prefix,track_name,track_color,
             track_background_color,track_url,tooltip]
        to_return.append(lst)
    return to_return

# executes this when run from command line
if __name__ == '__main__':
    about="Make annots.xml text."
    checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    # main comes from Quickload.py
    main(quickload_files)
