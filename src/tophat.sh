#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=16:00:00
cd $PBS_O_WORKDIR
module load bowtie2
module load tophat
G=A_thaliana_Jun_2009
O=SRP220157
if [ ! -d $O ]; then
    mkdir $O
fi

# S - sample name
# F - file name
# both need to be passed in using qsub -v option
# To run this run using qsub-doIt.sh, do:
#
#  qsub-doIt.sh .fastq.gz tophat.sh >jobs.out 2>jobs.err
#
tophat -p 8 -I 5000 -o $O/$S $G $F

