"""Script that identies retained intron reads within introns."""

"""
How this works:

First, identify all the annotated introns.
Make a bed file with all the annotated intron features.
Take the intersection of all of them.
