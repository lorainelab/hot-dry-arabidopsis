#!/bin/sh
# see:
# http://www.ncbi.nlm.nih.gov/sra?term=SRP026260
# http://www.ncbi.nlm.nih.gov/pubmed/24377444
# Arabidopsis drought memory study
mv SRR921316.fastq S3_2.fastq
mv SRR921315.fastq S3_1.fastq
mv SRR921314.fastq S1_2.fastq
mv SRR921313.fastq S1_1.fastq
mv SRR921312.fastq W_2.fastq
mv SRR921311.fastq W_1.fastq



