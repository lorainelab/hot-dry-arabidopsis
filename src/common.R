wet.col='#3366FF'
dry.col='#990000'
cool.col='#336600'
hot.col='#CC0033'

writeSamplesFile=function() {
  fname="../ExternalDataSets/samples.txt"
  groups=getGroups()
  colors=getColors(groups)
  fnames=paste0(names(groups),'.bam')
  N=length(fnames)
  samples=data.frame(sample=names(groups),
                     group=groups,
                     color=colors,
                     low=rep(F,N),
                     pcr=rep(F,N),
                     file=fnames)
  pcrDups=getPcrDups()
  samples[pcrDups,"pcr"]=T
  lowAbundance=getLowAbundance()
  samples[lowAbundance,"low"]=T
  write.table(samples,fname,row.names = F,
              sep=',',quote=F,col.names = T)
}

getSamples=function() {
  samples=c('CoolHT2','DryBT2','HotI1T1','HotK1T2','WetDT2','CoolL1T1',
            'DryCT1','HotI1T2','HotK2T1','WetFT1','CoolL1T2','DryCT2',
            'HotI2T1','HotK2T2','WetFT2','CoolL2T1','DryET1','HotI2T2',
            'WetAT1','DryBT1','DryET2','HotK1T1','WetAT2')
  samples=samples[order(samples)]
  o=c(grep('Cool.*T1',samples,value=T),
      grep('Hot.*T1',samples,value=T),
      grep('Cool.*T2',samples,value=T),
      grep('Hot.*T2',samples,value=T),
      grep('Wet.*T1',samples,value=T),
      grep('Dry.*T1',samples,value=T),
      grep('Wet.*T2',samples,value=T),
      grep('Dry.*T2',samples,value=T))
  o
}

# get the usable heat stress time point 1
# samples
getHotT1Samples=function() {
  samples=getSamples()
  pcrDups=getPcrDups()
  lowAbundance=getLowAbundance()
  samples=samples[!samples%in%pcrDups]
  samples=samples[!samples%in%lowAbundance]
  names(samples)=samples
  cool=grep('Cool.*T1',samples)
  hot=grep('Hot.*T1',samples)
  return(samples[c(cool,hot)])
}

# get the usable heat stress time point 2
# samples
getHotT2Samples=function() {
  samples=getSamples()
  pcrDups=getPcrDups()
  lowAbundance=getLowAbundance()
  samples=samples[!samples%in%pcrDups]
  samples=samples[!samples%in%lowAbundance]
  names(samples)=samples
  cool=grep('Cool.*T2',samples)
  hot=grep('Hot.*T2',samples)
  return(samples[c(cool,hot)])
}

getDroughtT2Samples=function(){
  samples=getSamples()
  pcrDups=getPcrDups()
  samples=samples[!samples%in%pcrDups]
  names(samples)=samples
  dry=grep('Dry.*T2',samples)
  wet=grep('Wet.*T2',samples)
#  wet=c(wet,grep('Cool',samples))
  return(samples[c(wet,dry)])
}

getAllControlSamples=function(){
  samples=getSamples()
  pcrDups=getPcrDups()
  samples=samples[!samples%in%pcrDups]
  names(samples)=samples
  wet=grep('Wet',samples)
  cool=grep('Cool',samples)
  return(samples[c(wet,cool)])
}

getGroups=function(labels=NULL){
  groups=c(rep('CoolT1',2),
           rep('HotT1',4),
           rep('CoolT2',2),
           rep('HotT2',4),
           rep('WetT1',2),
           rep('DryT1',3),
           rep('WetT2',3),
           rep('DryT2',3))
  names(groups)=getSamples()
  if (is.null(labels)) {return(groups)}
  else {return(groups[labels])}
}

# return list of samples with low read diversity,
# probably due to too much PCR
getPcrDups=function() {
  lst=c('WetAT1','WetFT1','WetAT2','DryBT1','DryET1')
  return(lst)
}

# return list of samples with low read abundance
getLowAbundance=function() {
  lst=c('DryBT1','DryCT1','DryET1','HotI1T1',
      'WetFT1','WetAT1','WetAT1')
  return(lst)
}

getDroughtDeFDR=function() {
  return(0.0001)
}

getHeatT1DeFDR=function() {
  return(0.005)
}

getHeatT2DeFDR=function(){
  return(0.005)
}

getAnnots=function() {
  annotsfile='../ExternalDataSets/genedescr.tsv.gz'
  annots=read.delim(annotsfile,header=T,sep='\t',quote="",as.is=T)
  rownames(annots)=annots$AGI
  return(annots)
}

# given a vector of AGI transcript codes, return
# AGI locus codes
varToLocusId=function(v) {
  sapply(strsplit(v,'\\.'),function(x)x[[1]])
}

getCounts=function(){
  fname='../Counts/results/heat_drought_counts.tsv.gz'
  d=read.delim(fname,header=T,sep='\t')
  row.names(d)=d$gene
  return(d)
}

# get normalized expression values for each gene, not including
# low coverage or high PCR duplicate samples
getRpkm=function(){
  fname='../Counts/results/heat_drought_RPKM.tsv.gz'
  d=read.delim(fname,header=T,sep='\t',quote='')
  row.names(d)=d$gene
  return(d)
}

# get normalized expression values for each gene, in each
# sample, including low coverage and high PCR duplicate
# samples
getRpkmAll=function(){
  fname='../Counts/results/heat_drought_RPKM_all.tsv.gz'
  d=read.delim(fname,header=T,sep='\t',quote='')
  row.names(d)=d$gene
  return(d)
}

getDroughtDiffExprResults=function(){
  fname='../DifferentialExpression/results/WetDry.txt.gz'
  d=read.delim(fname,header=T,sep='\t',quote='')
  row.names(d)=d$gene
  return(d)
}

getHeatT1DiffExprResults=function(){
  fname='../DifferentialExpression/results/CoolHotT1.txt.gz'
  d=read.delim(fname,header=T,sep='\t',quote='')
  row.names(d)=d$gene
  return(d)
}

getHeatT2DiffExprResults=function(){
  fname='../DifferentialExpression/results/CoolHotT2.txt.gz'
  d=read.delim(fname,header=T,sep='\t',quote='')
  row.names(d)=d$gene
  return(d)
}

# retrieve mapping of GO identifiers onto genes
getGeneToGOMapping=function(){
  # source('http://www.bioconductor.org/biocLite.R')
  # biocLite('org.At.tair.db')
  suppressPackageStartupMessages(library(org.At.tair.db))
  db=org.At.tairGO
  gene2go=toTable(db)[,c('gene_id','go_id')]
  tags=paste(gene2go$gene_id,gene2go$go_id)
  gene2go=gene2go[!duplicated(tags),]
  names(gene2go)=c('gene','category')
  return(gene2go)
}

# get a mapping of gene names (AGI codes) onto 
# pathway names and descriptions
# reads file made by MakeGeneToAracycPwy.Rmd
getGeneToAraCycMapping=function() {
  # source('http://www.bioconductor.org/biocLite.R')
  # biocLite('org.At.tairARACYC')
  # suppressPackageStartupMessages(library(org.At.tairARACYC))
  # library(org.At.tairARACYC)
  # db=org.At.tairARACYC
  # gene2pwy=toTable(db)
  # above code fails because library not available for any version
  # of R I can find
  fname='../GOSeq/results/agi2pwy.txt.gz'
  d=read.delim(fname,as.is=T,quote='',sep='\t',header=T)
  return(d)
}

getGeneToMapManMapping = function(){
  fname='../ExternalDataSets/Ath_AGI_LOCUS_TAIR10_Aug2012.txt.gz'
  gene2cat=read.delim(fname,as.is=T,header=T,sep='\t')  
  rmQuote=function(x){
    x=substr(x,2,nchar(x))
    x=substr(x,1,nchar(x)-1)
    return(x)
  }
  gene2cat$NAME=rmQuote(gene2cat$NAME)
  gene2cat$IDENTIFIER=rmQuote(gene2cat$IDENTIFIER)
  gene2cat$IDENTIFIER=toupper(gene2cat$IDENTIFIER)
  gene2cat=gene2cat[,c('NAME','IDENTIFIER')]
  key=paste(gene2cat$NAME,gene2cat$IDENTIFIER)
  gene2cat=gene2cat[!duplicated(key),]
  names(gene2cat)=c('category','gene')
  tokeep=grep('AT.G',gene2cat$gene)
  return(gene2cat[tokeep,])
}


# returns named vector containing size of largest transcript
# per gene, names are genes
getTxSizes=function(){
  fname='../ExternalDataSets/tx_size.txt.gz'
  sizes=read.delim(fname,sep='\t',header=T)
  genes=sizes$locus
  sizes=sizes$bp/1000
  names(sizes)=genes
  return(sizes)
}

# identify categories with unusually many or few
# DE genes
# de_FDR - fdr cutoff for deciding differential expression
# cat_FDR - fdr cutoff for deciding whether a categor contains unusually
#           many or few DE genes
# de_results - output of differential expression analysis
# gene2cat - data frame mapping genes onto category identifiers
testCategories=function(de_FDR=NULL,cat_FDR=NULL,
                de_results=NULL,gene2cat=NULL) {
  # source('http://www.bioconductor.org/biocLite.R')
  # biocLite('goseq')
  suppressPackageStartupMessages(library(goseq))
  gene.vector=rep(0,nrow(de_results))
  names(gene.vector)=de_results$gene
  v=which(de_results$fdr<=de_FDR)
  gene.vector[v]=1
  sizes=getTxSizes()
  sizes=sizes[names(gene.vector)]
  pwf=nullp(gene.vector,bias.data=sizes)
  r=goseq(pwf,gene2cat=gene2cat,method='Wallenius')
  names(r)[2:3]=c('over_fdr','under_fdr')
  r$over_fdr=p.adjust(r$over_fdr,method='BH')
  r$under_fdr=p.adjust(r$under_fdr,method='BH')
  o=order(r$over_fdr)
  r=r[o,]
  r=r[union(which(r$over_fdr<=cat_FDR),
            which(r$under_fdr<=cat_FDR)),]  
  r$percent_de=round(r$numDEInCat/r$numInCat*100,1)
  if (is.null(r$term)) { # sometimes category has id and description (term)
    r=r[,c('category','over_fdr','under_fdr','numDEInCat',
           'numInCat','percent_de')]
  }
  else {
    r=r[,c('category','over_fdr','under_fdr','numDEInCat',
           'numInCat','percent_de','term')]
  }
  return(r)
}



makeBarPlot=function(means,stdevs,ylim=NULL,...){
  oldlas=par('las')
  par(las=1)
  means=as.numeric(means)
  stdevs=as.numeric(stdevs)
  if (is.null(ylim)){
    indexes=which(means==max(means))
    maxval=max(means[indexes]+stdevs[indexes])
    maxval=1.05*maxval
    ylim=c(0,maxval)
  }
  mp=barplot(as.numeric(means),ylim=ylim,...)
  box()
  for (i in seq(1,length(means))) {
    segments(mp[i,1], means[i] - stdevs[i], mp[i,], means[i] + stdevs[i], lwd=2)
    # 1. The lower bar
    segments(mp[i,] - 0.1, means[i] - stdevs[i], mp[i,] + 0.1, means[i] - stdevs[i], lwd=2)
    # 2. The upper bar
    segments(mp[i,] - 0.1, means[i] + stdevs[i], mp[i,] + 0.1, means[i] + stdevs[i], lwd=2)
  }
  par(las=oldlas)
}

makeClusterPlot=function(hc,lab=hc$labels,
                         lab.col=rep(1,length(hc$labels)),
                          hang=0.1,axes=F, ...) {
  y = rep(hc$height,2)
  x = as.numeric(hc$merge)
  y = y[which(x<0)]
  x = x[which(x<0)]; x=abs(x)
  y = y[order(x)]; x = x[order(x)]
  plot(hc,labels=FALSE,hang=hang,axes=axes, ...)
  text(x=x, y=y[hc$order]-(max(hc$height)*hang),
       labels=lab[hc$order], col=lab.col[hc$order],
       srt=90, adj=c(1,0.5),xpd=NA, ... )
}

getColors=function(labels){
  colors=rep('',length(labels))
  colors[grep('Wet',labels)]=wet.col
  colors[grep('Dry',labels)]=dry.col
  colors[grep('Cool',labels)]=cool.col
  colors[grep('Hot',labels)]=hot.col
  return(colors)
}

getSplicingSupport=function(fname='../AltSplice/data/AS_heat_drought_wRI.txt.gz'){
  d=read.delim(fname,header=T,sep='\t',as.is=T)
  as.info=d[,1:7]
  as.id=paste(paste(as.info$chromosome,
                    paste(as.info$start,as.info$end,sep='-'),
                    sep=':'),
              as.info$strand,
              as.info$type)
  as.info$as.id=as.id
  as.info$len=as.info$end-as.info$start
  as.info=as.info[,c('as.id','Ga','Gp','chromosome','start',
                     'end','len',
                     'strand','type')]
  names(as.info)[2:3]=c('S','L')
  d=d[,8:ncol(d)]
  names(d)=sub('.sm','',names(d)) 
  names(d)=sub('Gp_','L.',names(d))
  names(d)=sub('Ga_','S.',names(d))
  d=cbind(as.info,d)
  return(d)
}

# returns %L (percent spliced in)
makeTestingFrame=function(d,threshold=15) {
  sampleStart=min(grep('S.',names(d)))
  newd=d[,1:sampleStart-1]
  for (i in seq(sampleStart,ncol(d)-1,by=2)) {
    colname=sub('S.','',names(d)[i])
    Tot=d[,i]+d[,i+1]
    indexes=Tot<threshold
    Tot[indexes]=NA
    Tot=d[,i+1]/Tot*100
    newd=cbind(newd,Tot)
    names(newd)[ncol(newd)]=colname
  }
  return(newd)
}

getSplicingPercentHotT1=function(fname='../AltSplice/results/hot1_testing_frame.txt.gz') {
  return(getDataAddGene(fname))
}



getSplicingPercentDry=function(fname='../AltSplice/results/dry_testing_frame.txt.gz') {
  return(getDataAddGene(fname))
}

getDataAddGene=function(fname) {
  d=read.delim(fname,header=T,as.is=T)
  d$gene=varToLocusId(d$L)
  return(d)
}

getBedHeads=function() {
  return(c('chr','start','end','name','score','strand','thickStart',
           'thickEnd','rgb','exons','lengths','starts','symbol','description'))
}
