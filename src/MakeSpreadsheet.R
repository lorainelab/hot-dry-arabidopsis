# read data file
getDat=function(f='results/DryAS.tsv.gz') {
  d=read.delim(f,as.is=T,header=T)
  d
}

# make IGB links (when you click, IGB goes there)
getIgbLinks=function(d,version='A_thaliana_Jun_2009') {
  uu=paste0('http://localhost:7085/UnibrowControl?version=',
            version,'&seqid=',d$chr,'&start=',d$start,
            '&end=',d$end)
  uu
}

# make links to google
getGoogleLinks=function(d) {
  agi=unlist(lapply(strsplit(d$L,'\\.'),function(x)x[[1]]))
  uu=paste0('http://www.google.com/search?q=',agi)
  uu  
}

# load xlsx library w/ lots of Java memory and AWT options set
# create a global variable (XLSX) to indicate this has already
# been done
getReadyToWriteXlxs=function() {
  if (!exists('XLSX')) {
    options(java.parameters="-Xmx2048m") # use 2 Gb of RAM for Java
    # this next line fixes a java issue on Mac
    Sys.setenv(NOAWT=1)
    require(xlsx)
    XLSX<<-'XLSX' # global variable assignment
  }
}

makeSpreadsheet=function(d,sheetName='Alt Splice',file=NULL) {
  getReadyToWriteXlxs()
  igblinks=getIgbLinks(d)
  googlelinks=getGoogleLinks(d)
  indexes=which(names(d)%in%c('chr','start','end'))
  nd=d[,-indexes]
  wb=createWorkbook(type="xlsx")
  sheet = createSheet(wb,sheetName=sheetName)
  colnamesStyle=CellStyle(wb,alignment=Alignment(h="ALIGN_CENTER"),
                          font=Font(wb,isBold=T))
  addDataFrame(nd,sheet,row.names=F,col.names=T,
               colnamesStyle=colnamesStyle,
               showNA=T,characterNA='NA')
  gene.index=which(names(nd)%in%c('gene','symbol'))
  as.id.index=which(names(nd)=='as.id')
  for (i in seq(1,nrow(nd))) {
    row=getRows(sheet,i+1)  # wb has an extra row for header
    #cell=getCells(row,1)[[as.id.index]]
    cell=getCells(row,as.id.index)[[1]]
    address=igblinks[i]
    addHyperlink(cell,address)
    cell=getCells(row,gene.index)[[1]]
    address=googlelinks[i]
    addHyperlink(cell,address)
  }
  if (!is.null(file)) {
    saveWorkbook(wb,file)
  }
  return(wb)
}

doHot1=function() {
  d=getDat('results/HotT1AS.txt')
  makeSpreadsheet(d,file='results/Hot1AS.xlsx')
}
doDry=function() {
  d=getDat('results/DryAS.txt')
  makeSpreadsheet(d,file='results/DryAS.xlsx')
}
