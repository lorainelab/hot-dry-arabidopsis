#!/bin/bash
# to run: 
#    qsub-tophat.sh >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 
SCRIPT=tophat.sh
G=A_thaliana_Jun_2009
O=hotdry
FS=$(ls *.fastq.gz)
for F in $FS
do
    S=$(basename $F .fastq.gz)
    # note: grep .err files for "Run complete"
    # to check that jobs finished
    if [ ! -f $O/$S/accepted_hits.bam ]; then
	CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,O=$O,G=$G,F=$F $SCRIPT"
	$CMD
    fi
done


