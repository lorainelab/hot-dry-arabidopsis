#!/bin/bash

# to run: 
#    qsub-find_junctions.sh >jobs.out 2>jobs.err
# to kill:
#    cat jobs.out | xargs qdel 

SCRIPT=find_junctions.sh
BAMS=$(ls *.bam)
for F in $BAMS
do
    S=$(basename $F .bam)
    CMD="qsub -N $S -o $S.out -e $S.err -vS=$S,F=$F $SCRIPT"
    $CMD
done
