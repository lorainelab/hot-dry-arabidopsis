#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=1
#PBS -l mem=96gb
#PBS -l walltime=6:00:00

# supplied by qsub -v options:
#   BED - name of bed file with gene models
#   F - number of bases flanking intron for junction support
#   RIF - number of bases inside intron for retained intron support

module load samtools
cd $PBS_O_WORKDIR
S=$(basename $BED .bed)
S=${S}_${F}_${RIF}
PreParse.py -s _$S *.FJ.bed.gz
ArabiTagMain.py -g $BED -f $F -s $S -e allJunctions_$S.bed
PostParse.py -b $BED AltSplicing_$S.abt AS_$S.txt
Add_RiGp_Support.py -f $RIF -d .. -i AS_$S.txt -o AS_${S}_wRI.txt
